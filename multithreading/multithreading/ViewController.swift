//
//  ViewController.swift
//  multithreading
//
//  Created by ashok kumar chejarla on 2/18/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        multithreading()
        asyncmultithreading()
        twoSerialQueuesAsync()
        oneSerialQueue()
        singleSerialQueueAsync()
        concurentQueueOne()
        concurrentSyncAndAsync()
    }
    
    func multithreading() {
        DispatchQueue.global().sync {
            print("Inside")
        }
        print("Outside")
        // Console output:
        // Inside
        // Outside
    }
    
    func asyncmultithreading() {
        DispatchQueue.global().async {
            print("Inside")
        }
        print("Outside")
        // Potential console output (based on QoS):
        // Outside
        // Inside
    }
    
    func twoSerialQueuesAsync() {
        let serial1 = DispatchQueue(label: "com.besher.serial1")
        let serial2 = DispatchQueue(label: "com.besher.serial2")

        serial1.async {
            for _ in 0..<5 { print("🔵") }
        }

        serial2.async {
            for _ in 0..<5 { print("🔴") }
        }
    }
    
    func oneSerialQueue() {
//        If we want to ensure the first loop finishes first before starting the second loop, we can submit the first task synchronously from the caller:
        let serial1 = DispatchQueue(label: "com.besher.serial1")
        let serial2 = DispatchQueue(label: "com.besher.serial2")

        serial1.sync { // <---- we changed this to 'sync'
            for _ in 0..<5 { print("🔵") } // this finishes first
        }
        // we don't get here until first loop terminates
        serial2.async {
            for _ in 0..<5 { print("🔴") }
        }
    }
    
    func  singleSerialQueueAsync() {
        let serial = DispatchQueue(label: "com.besher.serial")

        serial.async {
            for _ in 0..<5 { print("🔵") }
        }

        serial.async {
            for _ in 0..<5 { print("🔴") }
        }
        
        //output
//        🔵 //5 times
//        🔴 // 5 times
    }
    
    func concurentQueueOne() {
        let concurrent = DispatchQueue(label: "com.besher.concurrent", attributes: .concurrent)

        concurrent.async {
            for _ in 0..<5 { print("🔵") }
        }

        concurrent.async {
            for _ in 0..<5 { print("🔴") }
        }
    }
    
    func concurrentSyncAndAsync() {
        let concurrent = DispatchQueue(label: "com.besher.concurrent", attributes: .concurrent)

        concurrent.sync {
            for _ in 0..<5 { print("🔵") }
        }

        concurrent.async {
            for _ in 0..<5 { print("🔴") }
        }
    }
    
    

}

