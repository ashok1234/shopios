//
//  ViewModelProtocol.swift
//  ShopIos
//
//  Created by ashok kumar chejarla on 11/14/20.
//

import Foundation


protocol ViewModelProtocol: class {
    associatedtype Input
    associatedtype Output
}
