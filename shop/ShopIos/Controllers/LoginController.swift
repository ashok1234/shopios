//
//  LoginController.swift
//  ShopIos
//
//  Created by ashok kumar chejarla on 11/14/20.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

enum globalError: Error {
  case unWrapError
}

class LoginController: UIViewController, ControllerType, LoginViewModelDelegate {
    
    typealias ViewModelType = LoginControllerViewModel
    
    // MARK: - Properties
    private var viewModel: ViewModelType!
    private let disposeBag = DisposeBag()

    let rest = RestManager()
    
    // MARK: - UI
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configure(with: viewModel);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true);
        
    }
    
    
    @IBAction func signUpAction(_ sender: Any) {
        let signUpService = SignUpService()
        let signUpViewModel = SignUpViewModel(service: signUpService)
        let signUpViewController = SignUpViewController.create(with: signUpViewModel)
        
        present(signUpViewController, animated: true, completion: nil)
    }
    
    // MARK: - Functions
    func configure(with viewModel: ViewModelType) {
        viewModel.delegate = self
//        viewModel.createUser();
        
            emailTextfield.rx.text.asObservable()
                .ignoreNil()
                .subscribe(viewModel.input.email)
                .disposed(by: disposeBag)
            
            passwordTextfield.rx.text.asObservable()
                .ignoreNil()
                .subscribe(viewModel.input.password)
                .disposed(by: disposeBag)
            
            signInButton.rx.tap.asObservable()
                .subscribe(viewModel.input.signInDidTap)
                .disposed(by: disposeBag)
        
//            registerButton.rx.tap.asObservable()
//                .subscribe(viewModel.input.signUpDidTap)
//                .disposed(by: disposeBag)
            
            viewModel.output.errorsObservable
                .subscribe(onNext: { [unowned self] (error) in
                    print("error")
                    print(error)
                    self.presentError(error)
                })
                .disposed(by: disposeBag)
            
            viewModel.output.loginResultObservable
                .subscribe(onNext: { [unowned self] (user) in
                    DispatchQueue.main.async {
                        try? self.presentViewCotroller();
                    }
                   
                })
                .disposed(by: disposeBag)

    }
    
    func presentViewCotroller() throws {
        // Create a reference to the the appropriate storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let dashboardViewModel = DashboardViewModel();
        guard let dashboardViewController = storyboard.instantiateViewController(withIdentifier: "dashboardViewController") as? DashboardViewController else {
            throw globalError.unWrapError;
        }
        
        dashboardViewController.viewModel = dashboardViewModel;
        
        let cartViewModel = CartViewModel();
        guard let shoppingCartViewController = storyboard.instantiateViewController(withIdentifier: "shoppingCartViewController") as? ShoppingCartViewController else {
            throw globalError.unWrapError;
        }
        shoppingCartViewController.viewModel = cartViewModel;
        
        let tabBarController = UITabBarController()

        tabBarController.viewControllers = [dashboardViewController, shoppingCartViewController];
        tabBarController.selectedIndex = 1;
        UIApplication.shared.windows.first?.rootViewController = tabBarController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension LoginController {
    
    func success() {
        print("this is LoginController")
        print("success")
        print(viewModel.jobUser ?? "this is jobUser");
    }
    
    func failure(error: Error) {
        print("this is failure");
        print(error)
    }

    static func create(with viewModel: ViewModelType) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginController") as! LoginController
        controller.viewModel = viewModel
        return controller
    }
    
    func getUsersList() {
        guard let url = URL(string: "https://reqres.in/api/users") else { return }
        
        // The following will make RestManager create the following URL:
        // https://reqres.in/api/users?page=2
        // rest.urlQueryParameters.add(value: "2", forKey: "page")
        
        rest.makeRequest(toURL: url, withHttpMethod: .get) { (results) in
            if let data = results.data {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                guard let userData = try? decoder.decode(UserData.self, from: data) else { return }
                print(userData.description)
            }
            
            print("\n\nResponse HTTP Headers:\n")
            
            if let response = results.response {
//                for (key, value) in response.headers.allValues() {
////                    print(key, value)
//                }
            }
        }
    }
    
    
    func getNonExistingUser() {
        guard let url = URL(string: "https://reqres.in/api/users/100") else { return }
        
        rest.makeRequest(toURL: url, withHttpMethod: .get) { (results) in
            if let _ = results.data, let response = results.response {
                if response.httpStatusCode != 200 {
                    print("\nRequest failed with HTTP status code", response.httpStatusCode, "\n")
                }
            }
        }
    }
    
    func getSingleUser() {
        guard let url = URL(string: "https://reqres.in/api/users/1") else { return }
        
        rest.makeRequest(toURL: url, withHttpMethod: .get) { (results) in
            if let data = results.data {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                guard let singleUserData = try? decoder.decode(SingleUserData.self, from: data),
                    let user = singleUserData.data,
                    let avatar = user.avatar,
                    let url = URL(string: avatar) else { return }
                
                self.rest.getData(fromURL: url, completion: { (avatarData) in
                    guard let avatarData = avatarData else { return }
                    let cachesDirectory = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
                    let saveURL = cachesDirectory.appendingPathComponent("avatar.jpg")
                    try? avatarData.write(to: saveURL)
                    print("\nSaved Avatar URL:\n\(saveURL)\n")
                })
                
            }
        }
    }
}
