//
//  SignUpViewController.swift
//  ShopIos
//
//  Created by ashok kumar chejarla on 11/17/20.
//

import UIKit
import RxSwift
import RxCocoa

class SignUpViewController: UIViewController, SignUpViewModelDelegate {
    
//    typealias ViewModelType = SignUpViewModel
    
    // MARK: - Properties
    private var viewModel: SignUpViewModel!
    private let disposeBag = DisposeBag()

    let rest = RestManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("viewdidload")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("this is viewDidAppear")
        guard let viewModel = viewModel else {
            print("viewMOdel is nii")
            return
        }
        
        print(viewModel)
        configure(with: viewModel)
    }
    
    @IBOutlet weak var signUpBtn: UIButton!
    
    @IBAction func signUp(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Functions
    func configure(with viewModel: SignUpViewModel) {
        viewModel.delegate = self
//        viewModel.createUser();
        
        
        signUpBtn.rx.tap.asObservable()
                .subscribe(viewModel.input.signUpDidTap)
                .disposed(by: disposeBag)
            
            viewModel.output.errorsObservable
                .subscribe(onNext: { [unowned self] (error) in
                    print("error")
                    print(error)
                    self.presentError(error)
                })
                .disposed(by: disposeBag)
            
            viewModel.output.signUpResultObservable
                .subscribe(onNext: { [unowned self] (user) in
                    self.presentMessage("User successfully signed in")
                })
                .disposed(by: disposeBag)

    }
    
}

extension SignUpViewController {
    
    func success() {
        print("this is LoginController")
        print("success")
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
       
//        print(viewModel.jobUser ?? "this is jobUser");
    }
    
    func failure(error: Error) {
        print("this is failure");
        print(error)
    }
    
    static func create(with viewModel: SignUpViewModel?) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        print("this is viewModel")
        
        guard let viewModel = viewModel else {
            print("assignment nill")
            return UIViewController()
        }
        
        print(viewModel)
        controller.viewModel = viewModel
        return controller
    }
}
