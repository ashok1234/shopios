//
//  SignUpService.swift
//  ShopIos
//
//  Created by ashok kumar chejarla on 11/17/20.
//

import Foundation
import RxSwift

protocol SignUpServiceProtocol {
    func createUser() -> Observable<User?>
}

class SignUpService: SignUpServiceProtocol {
    
    let rest = RestManager()
    func createUser() -> Observable<User?> {
        print("create user");

        return Observable.create { observer in
            let urlStr = ApiConstants.baseUrl + ApiConstants.createUser
            guard let url = URL(string: urlStr) else {
                observer.onError(ApiError.invalidUrl)
                return Disposables.create()
            }
            
            self.rest.requestHttpHeaders.add(value: "application/json", forKey: "Content-Type")
            
            
//            for (key, value) in params {
    //            print("kind: \(key)")
            self.rest.httpBodyParameters.add(value: "test123@gmail.com", forKey: "email")
            self.rest.httpBodyParameters.add(value: "password", forKey: "password")
            self.rest.httpBodyParameters.add(value: "test first", forKey: "firstName")
            self.rest.httpBodyParameters.add(value: "test last", forKey: "lastName")
            self.rest.httpBodyParameters.add(value: "3022684496", forKey: "phone")
            self.rest.httpBodyParameters.add(value: "admin", forKey: "role")
            
            
//            }
            print(url);
            
            self.rest.makeRequest(toURL: url, withHttpMethod: .post) { (results) in
                guard let response = results.response else {
                    observer.onError(ApiError.invalidUrl)
                    var _ = Disposables.create()
                    return
                }
                
                
                
                if response.httpStatusCode == 200 {
                    guard let data = results.data else {
                         observer.onError(ApiError.invalidUrl)
                        var _ = Disposables.create()
                        return
                    }
                    let decoder = JSONDecoder()
                    print("data is signup")
                    print(data)
                    
                    do {
                    
                        guard let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? Array<Any> else {
                                print("jsonResult")
                                return
                            }
//                        let data1 =  try JSONSerialization.data(withJSONObject: jsonResult, options: .withoutEscapingSlashes) // first of all convert json to the data
                        
//                        let convertedString = String(data: data1, encoding: String.Encoding.utf8)
                         
                                // Parse JSON data
                                print("this is jsonResult")
                        print(jsonResult)
//                        jsonResult.data(using: .utf8)
//                        let json = jsonResult.da

//                        var users : [User] = [];
                        guard  let user = try? decoder.decode([User].self, from: data) else {
                            print("error")
                            return
                        }
                        
                        print(user);
//                        print(user.description)
                            observer.onNext(user[0])
                            
                        
                    }  catch {
                        print(error.localizedDescription)
                    }
                    
                }
            }

             // Simulation of successful user authentication.
            return Disposables.create()
        }
        
    }
}
