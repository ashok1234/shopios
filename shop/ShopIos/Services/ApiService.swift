//
//  ApiService.swift
//  ShopIos
//
//  Created by ashok kumar chejarla on 11/16/20.
//

import Foundation
import RxSwift


//protocol ApiServiceProtocol {
//    func signIn(with urlString, Params: )
//}
//
class ApiService {
    var baseUrl: String = "http://localhost:8080/"
    var login = "login";
//    func signIn(with credentials: Credentials)  {
//        return Observable.create { observer in
//            /*
//             Networking logic here.
//            */
//            observer.onNext(User()) // Simulation of successful user authentication.
//            return Disposables.create()
//        }
//    }
}

struct ApiConstants {
    static let appURL = "https://reqres.in/api/"
    static let createUser = "SignUp/save"
    static let  baseUrl = "http://192.168.1.89:8080/"
    static let login = "login";
}

enum ApiError: Error {
    case invalidEmail
    case invalidPassword
    case invalidPhoneNumber
    case invalidUrl
}

extension ApiError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .invalidEmail:
            return NSLocalizedString("Description of invalid email address", comment: "Invalid Email")
        case .invalidPassword:
            return NSLocalizedString("Description of invalid password", comment: "Invalid Password")
        case .invalidPhoneNumber:
            return NSLocalizedString("Description of invalid phoneNumber", comment: "Invalid Phone Number")
        case .invalidUrl:
            return NSLocalizedString("Description of invalid Url", comment: "Invalid Url")
        }
    }
}
