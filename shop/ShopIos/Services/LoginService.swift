//
//  LoginService.swift
//  ShopIos
//
//  Created by ashok kumar chejarla on 11/14/20.
//

import Foundation
import RxSwift

protocol LoginServiceProtocol {
    func signIn(with credentials: Credentials) -> Observable<User>
    func createUser(params: Credentials) -> Observable<JobUser?>
}

enum httpError: Error {
    case invalidSelection
}

class LoginService: LoginServiceProtocol {
    
    
    let rest = RestManager()
    func signIn(with credentials: Credentials) -> Observable<User> {
        return Observable.create { observer in
            let urlStr = ApiConstants.baseUrl + ApiConstants.login
            guard let url = URL(string: urlStr) else {
                observer.onError(ApiError.invalidUrl)
                return Disposables.create()
            }
            
//            print("credentials");
//            print(credentials);
//
            self.rest.requestHttpHeaders.add(value: "application/json", forKey: "Content-Type")
            
            self.rest.httpBodyParameters.add(value: credentials.email, forKey: "username")
            self.rest.httpBodyParameters.add(value: credentials.password, forKey: "password")
            
            self.rest.makeRequest(toURL: url, withHttpMethod: .post) { (results) in
                print(results);
                guard let response = results.response else {
                    observer.onError(ApiError.invalidUrl)
                    var _ = Disposables.create()
                    return
                }
                
                if response.httpStatusCode == 200 {
                    guard let data = results.data else {
                         observer.onError(ApiError.invalidUrl)
                        var _ = Disposables.create()
                        return
                    }
                    let decoder = JSONDecoder()
//                    print("data")
//                    print(data)
                    
                     let JSONString = String(data: data, encoding: String.Encoding.utf8);
                        
                    print(JSONString ?? "");
                
                    guard let jobUser = try? decoder.decode(User.self, from: data) else {
                        observer.onError(ApiError.invalidUrl)
                        var _ = Disposables.create()
                        return
                    }
                    
                    observer.onNext(jobUser)
                    
//                    print(jobUser.description)
                }
            }


            /*
             Networking logic here.
            */
//            observer.onNext(User()) // Simulation of successful user authentication.
            return Disposables.create()
        }
    }
    
    func createUser(params: Credentials) -> Observable<JobUser?> {
        print("create user");
        var params = params;
        
        params.email = "ashokch8@gmail.com";
        params.password = "password";
            
        return Observable.create { observer in
            let urlStr = ApiConstants.baseUrl + ApiConstants.createUser
            guard let url = URL(string: urlStr) else {
                observer.onError(ApiError.invalidUrl)
                return Disposables.create()
            }
            
            self.rest.requestHttpHeaders.add(value: "application/json", forKey: "Content-Type")
            
            
//            for (key, value) in params {
    //            print("kind: \(key)")
            self.rest.httpBodyParameters.add(value: "test123@gmail.com", forKey: "email")
            self.rest.httpBodyParameters.add(value: params.password, forKey: "password")
            self.rest.httpBodyParameters.add(value: "test first", forKey: "firstName")
            self.rest.httpBodyParameters.add(value: "test last", forKey: "lastName")
            self.rest.httpBodyParameters.add(value: "3022684496", forKey: "phone")
            self.rest.httpBodyParameters.add(value: "admin", forKey: "role")
            
            
//            }
            print(url);
            
            self.rest.makeRequest(toURL: url, withHttpMethod: .post) { (results) in
                guard let response = results.response else {
                    observer.onError(ApiError.invalidUrl)
                    var _ = Disposables.create()
                    return
                }
                
                if response.httpStatusCode == 201 {
                    guard let data = results.data else {
                         observer.onError(ApiError.invalidUrl)
                        var _ = Disposables.create()
                        return
                    }
                    let decoder = JSONDecoder()
                    print("data")
                    print(data)
                    guard let jobUser = try? decoder.decode(JobUser.self, from: data) else {
                        observer.onError(ApiError.invalidUrl)
                        var _ = Disposables.create()
                        return
                    }
                    
                    observer.onNext(jobUser)
                    
                    print(jobUser.description)
                }
            }

             // Simulation of successful user authentication.
            return Disposables.create()
        }
        
    }
}
