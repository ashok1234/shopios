//
//  Credentials.swift
//  ShopIos
//
//  Created by ashok kumar chejarla on 11/14/20.
//

import Foundation

struct Credentials {
    var email: String
    var password: String
}

struct User: Codable {
    
    var userId: Int?
    var username: String?
    var email: String?
    var access_token: String?
    var token_type: String?
    var expires_in: Int?
    
    enum CodingKeys : String, CodingKey {
        case userId
        case username
        case email
        case access_token
        case token_type
        case expires_in
    }
}
