//
//  SignUpViewModel.swift
//  ShopIos
//
//  Created by ashok kumar chejarla on 11/17/20.
//

import Foundation
import RxSwift

protocol SignUpViewModelDelegate {
    // Classes that adopt this protocol MUST define
    // this method -- and hopefully do something in
    // that definition.
    func success();
    func failure(error: Error);
}

class SignUpViewModel: ViewModelProtocol {
    
    var service: SignUpServiceProtocol;
    var delegate: SignUpViewModelDelegate?
    var jobUser: User?
    
    var input: Input
    var output: Output
    
    // MARK: - Private properties
    private let signUpDidTapSubject = PublishSubject<Void>()
    private let signUpResultSubject = PublishSubject<User>()
    private let errorsSubject = PublishSubject<Error>()
    private let disposeBag = DisposeBag()
    
    
    struct Input {
        let signUpDidTap: AnyObserver<Void>
    }
    struct Output {
        let signUpResultObservable: Observable<User>
        let errorsObservable: Observable<Error>
    }
    
    
    init(service: SignUpServiceProtocol) {
        self.service = service
        
        input = Input(signUpDidTap: signUpDidTapSubject.asObserver())
        output = Output(signUpResultObservable: signUpResultSubject.asObservable(),
                        errorsObservable: errorsSubject.asObservable())
        
        signUpDidTapSubject.flatMapLatest {
            return self.service.createUser()
        }.subscribe(onNext: { result in
                            self.jobUser = result
                            print("onNext:", result ?? "")
                            self.delegate?.success();
            
                                }, onError: { error in
                                    print("onError:", error)
                                    self.delegate?.failure(error: error)
                                })
                                .disposed(by: disposeBag)
        
//        signUpDidTapSubject
//                    .withLatestFrom(credentialsObservable)
//                    .flatMapLatest { credentials in
//                        return self.service.createUser(params: credentials)
//                    }
//            .subscribe(onNext: { result in
//                self.jobUser = result
//                print("onNext:", result ?? "")
//                self.delegate?.success();
//
//                    }, onError: { error in
//                        print("onError:", error)
//                        self.delegate?.failure(error: error)
//                    })
//                    .disposed(by: disposeBag)
        
    }

}
