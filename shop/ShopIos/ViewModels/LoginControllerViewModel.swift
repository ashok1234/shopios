//
//  LoginControllerViewModel.swift
//  ShopIos
//
//  Created by ashok kumar chejarla on 11/14/20.
//

import RxSwift

protocol LoginViewModelDelegate {
    // Classes that adopt this protocol MUST define
    // this method -- and hopefully do something in
    // that definition.
    func success();
    func failure(error: Error);
}
class LoginControllerViewModel: ViewModelProtocol {
    
    var service: LoginServiceProtocol;
    var delegate: LoginViewModelDelegate?
    var jobUser: JobUser?
    
    struct Input {
        let email: AnyObserver<String>
        let password: AnyObserver<String>
        let signInDidTap: AnyObserver<Void>
        let signUpDidTap: AnyObserver<Void>
    }
    struct Output {
        let loginResultObservable: Observable<User>
        let errorsObservable: Observable<Error>
    }
    
    let input: Input
    let output: Output
    
    // MARK: - Private properties
    private let emailSubject = PublishSubject<String>()
    private let passwordSubject = PublishSubject<String>()
    private let signInDidTapSubject = PublishSubject<Void>()
    private let signUpDidTapSubject = PublishSubject<Void>()
    private let loginResultSubject = PublishSubject<User>()
    private let errorsSubject = PublishSubject<Error>()
    private let disposeBag = DisposeBag()
    
    private var credentialsObservable: Observable<Credentials> {
        return Observable.combineLatest(emailSubject.asObservable(), passwordSubject.asObservable()) { (email, password) in
            return Credentials(email: email, password: password)
        }
    }

    init(_ loginService: LoginServiceProtocol) {
        service = loginService;
        input = Input(email: emailSubject.asObserver(),
                      password: passwordSubject.asObserver(),
                      signInDidTap: signInDidTapSubject.asObserver(),
                      signUpDidTap: signUpDidTapSubject.asObserver()
                      )
        
        output = Output(loginResultObservable: loginResultSubject.asObservable(),
                        errorsObservable: errorsSubject.asObservable())
        
        signInDidTapSubject
                    .withLatestFrom(credentialsObservable)
                    .flatMapLatest { credentials in
                        return loginService.signIn(with: credentials).materialize()
                    }
                    .subscribe(onNext: { [weak self] event in
                        switch event {
                        case .next(let user):
                            self?.loginResultSubject.onNext(user)
                        case .error(let error):
                            self?.errorsSubject.onNext(error)
                        default:
                            break
                        }
                    })
                    .disposed(by: disposeBag)
        
        signUpDidTapSubject
                    .withLatestFrom(credentialsObservable)
                    .flatMapLatest { credentials in
                        return self.service.createUser(params: credentials)
                    }
            .subscribe(onNext: { result in
                self.jobUser = result
//                print("onNext:", result ?? "")
                self.delegate?.success();

                    }, onError: { error in
                        print("onError:", error)
                        self.delegate?.failure(error: error)
                    })
                    .disposed(by: disposeBag)
    }
    
//    func createUser(with: credentials) {
//        let dic = ["name" : "John", "job" : "Developer"];
//        service.createUser(params: dic)
//            .subscribe(onNext: { result in
//                self.jobUser = result
//                print("onNext:", result ?? "")
//                self.delegate?.success();
//
//                    }, onError: { error in
//                        print("onError:", error)
//                        self.delegate?.failure(error: error)
//                    })
//                    .disposed(by: disposeBag)
//    }
}
